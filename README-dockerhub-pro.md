# Orthanc for Docker
Docker image with [Orthanc](http://www.orthanc-server.com/) and its official plugins (including commercial plugins). Orthanc is a lightweight, RESTful Vendor Neutral Archive for medical imaging.

Full documentation is available [here](https://osimis.atlassian.net/wiki/spaces/OKB/pages/26738689/How+to+use+osimis+orthanc+Docker+images).

Sample setups using this image are available [here](https://bitbucket.org/osimis/orthanc-setup-samples/).

Release notes are available [here](https://bitbucket.org/osimis/orthanc-builder/src/master/release-notes-docker-images.txt)


# packages content

#### 18.12.3
```

component                             version
---------------------------------------------
Orthanc server                        1.5.1
Osimis Web viewer plugin              1.2.0
Osimis Web viewer plugin (alpha)      7c4d1a44
Modality worklists plugin             1.5.1
Serve folders plugin                  1.5.1
Orthanc Web viewer plugin             2.4
DICOMweb plugin                       0.5
PostgreSQL plugin                     2.2
MySQL plugin                          1.1
WSI Web viewer plugin                 0.5
Authorization plugin                  0.2.2

MSSql plugin                          1.0.0
Azure Storage plugin (using blobs)    0.3.2
Osimis Web viewer pro plugin          1.2.0.0
Osimis Web viewer pro plugin (alpha)  6b11ef5
```
